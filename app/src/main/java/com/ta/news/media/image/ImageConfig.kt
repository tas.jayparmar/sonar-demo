package com.ta.news.media.image

import android.os.Environment
import com.ta.news.media.image.ImagePicker.ComperesLevel
import java.io.Serializable

/**
 * Created by Alhazmy13 on 8/16/16.
 * MediaPicker
 */
class ImageConfig : Serializable {
    var extension: ImagePicker.Extension
    var compressLevel: ComperesLevel
    var mode: ImagePicker.Mode
    var directory: String
    var reqHeight: Int
    var reqWidth: Int
    var allowMultiple: Boolean
    var isImgFromCamera = false
    var allowOnlineImages: Boolean
    var debug = false
    override fun toString(): String {
        return "ImageConfig{" +
                "extension=" + extension +
                ", compressLevel=" + compressLevel +
                ", mode=" + mode +
                ", directory='" + directory + '\'' +
                ", reqHeight=" + reqHeight +
                ", reqWidth=" + reqWidth +
                ", allowMultiple=" + allowMultiple +
                ", isImgFromCamera=" + isImgFromCamera +
                ", allowOnlineImages=" + allowOnlineImages +
                ", debug=" + debug +
                '}'
    }

    init {
        this.extension = ImagePicker.Extension.PNG
        compressLevel = ComperesLevel.NONE
        mode = ImagePicker.Mode.CAMERA
        directory = Environment.getExternalStorageDirectory().toString() + ImageTags.Tags.IMAGE_PICKER_DIR
        reqHeight = 0
        reqWidth = 0
        allowMultiple = false
        allowOnlineImages = false
    }
}