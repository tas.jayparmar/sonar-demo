package com.ta.news.pojo

import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table

/**
 * Created by arthtilva on 26-Feb-17.
 */
@Table(name = "OfflineNews")
class OfflineNews : Model() {
    @Column(name = "postId")
    var postId = 0

    @Column(name = "newsTitle")
    var newsTitle: String = ""

    @Column(name = "newsDetails")
    var newsDetails: String = ""

    @Column(name = "address")
    var address: String = ""

    @Column(name = "addDate")
    var addDate: String = ""

    @Column(name = "firstName")
    var firstName: String = ""

    @Column(name = "lastName")
    var lastName: String = ""

    @Column(name = "mobileNo")
    var mobileNo: String = ""

    @Column(name = "profileImage")
    var profileImage: String = ""

    @Column(name = "userId")
    var userId = -1

    @Column(name = "isVerified")
    var isVerified = false

    @Column(name = "approxPrice")
    var approxPrice: String = ""
}

@Table(name = "OfflineMedia")
class OfflineMedia : Model() {
    @Column(name = "postId")
    var postId = 0

    @Column(name = "mediaName")
    var mediaName: String = ""

    @Column(name = "folderPath")
    var folderPath: String = ""

    @Column(name = "thumbFolderPath")
    var thumbFolderPath: String? = ""

    @Column(name = "thumbImage")
    var thumbImage: String? = ""

    @Column(name = "type")
    var type = 0
}