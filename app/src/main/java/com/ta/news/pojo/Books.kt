package com.ta.news.pojo

import java.io.Serializable
import java.util.*

/**
 * Created by arthtilva on 07-Nov-16.
 */
class Books {
    var data: List<BooksPojo> = ArrayList()
    var totalPages = 0
    var success = false
    var base_url: String = ""
}

class BooksPojo : Serializable {
    var isLoader = false
    var id = 0
    var title: String = ""
    var description: String = ""
    var price: String = ""
    var titleImage: String = ""
    var folderPath: String = ""
    var bookImages = ArrayList<MediaPOJO>()


    constructor(loader: Boolean) {
        isLoader = loader
    }
}