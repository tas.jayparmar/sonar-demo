package com.ta.news.activity.auth

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.activity.MainActivity
import com.ta.news.activity.TermsAndConditionsActivity
import com.ta.news.controls.CustomDialog
import com.ta.news.utils.Constants
import com.ta.news.utils.RetrofitHelper
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_register.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response


class RegisterActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)

        edMobile.setText(storeUserData.getString(Constants.USER_COUNTRY_CODE) + storeUserData.getString(Constants.USER_PHONE_NUMBER))
        btnRegister.setOnClickListener {
            if (Utils.isEmpty(edFirstName)) {
                showAlert(activity, getString(R.string.enter_name))
            } else if (Utils.isEmpty(edLastName)) {
                showAlert(activity, getString(R.string.enter_last_name))
            } else if (edEmail.text.toString().isNotEmpty() && !Utils.isValidEmail(edEmail)) {
                showAlert(activity, getString(R.string.enter_valid_email))
            } else if (Utils.isEmpty(edMobile)) {
                showAlert(activity, getString(R.string.enter_mobile_number))
            } else if (Utils.isEmpty(edAddress)) {
                showAlert(activity, getString(R.string.enter_address))
            } else if (Utils.isEmpty(edCity)) {
                showAlert(activity, getString(R.string.enter_city))
            } else if (Utils.isEmpty(edTaluka)) {
                showAlert(activity, getString(R.string.enter_taluka))
            } else if (Utils.isEmpty(edPincode)) {
                showAlert(activity, getString(R.string.enter_pincode))
            } else if (!cbTerms.isChecked) {
                showAlert(activity, getString(R.string.accept_terms))
            } else if (!Utils.isOnline(activity)) {
                internetAlert(activity)
            } else {
                btnRegister.isEnabled = false
                register()
            }
        }
        btnTermsAndConditions.setOnClickListener { v: View? -> startActivity(Intent(activity, TermsAndConditionsActivity::class.java)) }
        getProfile()
        //getDistrictList()
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val deviceToken = instanceIdResult.token
            Log.i("token", deviceToken)
            storeUserData.setString(Constants.USER_FCM, deviceToken)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun getProfile() {
        showProgress()
        val retrofitHelper = RetrofitHelper(activity)
        var call: Call<ResponseBody> =
                retrofitHelper.api().getProfileByMobile(storeUserData.getString(Constants.USER_COUNTRY_CODE), storeUserData.getString(Constants.USER_PHONE_NUMBER))
        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                try {
                    val responseData = body.body()!!.string()
                    Log.i("response", "onResponse: $responseData")
                    if (!TextUtils.isEmpty(responseData)) {
                        val jsonObject = JSONObject(responseData)
                        if (jsonObject.getBoolean("success")) {
                            edFirstName.setText(jsonObject.getString("firstName"))
                            edLastName.setText(jsonObject.getString("lastName"))
                            edEmail.setText(jsonObject.getString("email"))
                            edAddress.setText(jsonObject.getString("address"))
                            edCity.setText(jsonObject.getString("city"))
                            edTaluka.setText(jsonObject.getString("taluka"))
                            edPincode.setText(jsonObject.getString("pincode"))
                        }
                    }
                } catch (e: Exception) {
                    var bundle = Utils.getUserDetails(activity)
                    bundle.putString("error", e.message.toString())
                    bundle.putString("exception", "Exception")
                    FirebaseAnalytics.getInstance(activity).logEvent("getProfile", bundle)
                }
            }

            override fun onError(code: Int, error: String) {
                dismissProgress()
            }
        })
    }

    private fun register() {
        showProgress()
        if (storeUserData.getString(Constants.USER_FCM).isEmpty()) {
            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    if (task.result != null) {
                        storeUserData.setString(Constants.USER_FCM, task.result!!.token)
                    }
                }
            }
        }
        val retrofitHelper = RetrofitHelper(activity)
        var call: Call<ResponseBody> =
                retrofitHelper.api().register(
                        edFirstName.text.toString().trim(),
                        edLastName.text.toString().trim(),
                        edEmail.text.toString().trim(),
                        storeUserData.getString(Constants.USER_COUNTRY_CODE),
                        storeUserData.getString(Constants.USER_PHONE_NUMBER),
                        edAddress.text.toString().trim(),
                        edCity.text.toString().trim(),
                        edTaluka.text.toString().trim(),
                        edPincode.text.toString().trim(),
                        storeUserData.getString(Constants.USER_FCM),
                        "android", "true"
                )
        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseData = body.body()!!.string()
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        storeUserData.setString(Constants.USER_ID, jsonObject.getString("userId"))
                        storeUserData.setString(Constants.USER_FIRST_NAME, edFirstName.text.toString())
                        storeUserData.setString(Constants.USER_LAST_NAME, edLastName.text.toString())
                        storeUserData.setString(Constants.USER_EMAIL, edEmail.text.toString())
                        storeUserData.setString(Constants.USER_ADDRESS, edAddress.text.toString())
                        storeUserData.setString(Constants.USER_CITY, edCity.text.toString())
                        storeUserData.setString(Constants.USER_TALUKA, edTaluka.text.toString())
                        storeUserData.setString(Constants.USER_PINCODE, edPincode.text.toString())

                        storeUserData.setString(Constants.REASON_MESSAGE, jsonObject.optString("reasonMessage"))
                        storeUserData.setInt(Constants.USER_STATUS, jsonObject.getInt("status"))
                        storeUserData.setBoolean(Constants.IS_PREMIUM, jsonObject.getInt("isPremium") == 1)

                        if (storeUserData.getInt(Constants.USER_STATUS) == 0) {
                            storeUserData.clearData(activity)
                            val dialog = CustomDialog(activity)
                            dialog.show()
                            dialog.setCancelable(false)
                            dialog.setMessage(storeUserData.getString(Constants.REASON_MESSAGE))
                            dialog.setPositiveButton(R.string.ok) {
                                startActivity(Intent(activity, MainActivity::class.java))
                                finish()
                            }
                        } else {
                            startActivity(Intent(activity, MainActivity::class.java))
                            finish()
                        }
                    } else {
                        btnRegister.isEnabled = true
                        Snackbar.make(btnRegister, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show()
                    }
                }
            }

            override fun onError(code: Int, error: String) {
                btnRegister.isEnabled = true
                dismissProgress()
            }
        })
    }

    /*private fun getDistrictList() {
        val retrofitHelper = RetrofitHelper(activity)
        var call: Call<ResponseBody> =
                retrofitHelper.api().getDistrict()
        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                val res = body.body()!!.string()
                Log.i("response", "onResponse: $res")
                val reader: Reader = StringReader(res)
                val gson = GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .serializeNulls()
                        .create()

                val district = gson.fromJson(reader, District::class.java)
                if (district != null) {
                    if (district.success && district.data.size > 0) {

                    } else {
                        showAlert(activity, district.message)
                    }
                }
            }

            override fun onError(code: Int, error: String) {
            }
        })
    }*/
}