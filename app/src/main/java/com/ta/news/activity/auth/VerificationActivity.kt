package com.ta.news.activity.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_verification.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

class VerificationActivity : BaseActivity() {
    val TAG = this.javaClass.name
    var sentTime: Long = 0
    private var mVerificationId: String = ""
    private var sessionId: String = ""
    private var mAuth: FirebaseAuth? = null
    var countryCode = ""
    var phoneNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        activity = this
        storeUserData = StoreUserData(activity)
        mAuth = FirebaseAuth.getInstance()
        countryCode = intent.getStringExtra("countryCode")
        phoneNumber = intent.getStringExtra("phoneNumber")

        firebasePhoneAuth(countryCode + phoneNumber)
        buttonResend.setOnClickListener {
            if (System.currentTimeMillis() - sentTime > 30000) {
                mVerificationId = ""
                sendOtp()
                sentTime = System.currentTimeMillis()
            } else {
                showAlert(activity, getString(R.string.please_wait_for_30_sec))
            }
        }
        buttonVerifyPhone.setOnClickListener {
            if (Utils.isEmpty(fieldVerificationCode)) {
                showAlert(activity, getString(R.string.code_sent))
            } else {
                if (mVerificationId.isNotEmpty()) {
                    val credential = PhoneAuthProvider.getCredential(mVerificationId, fieldVerificationCode.text.toString())
                    signInWithPhoneAuthCredential(credential)
                } else if (storeUserData.getString(Constants.SESSION_ID).isNotEmpty()) {
                    verifyOTP()
                } else {
                    showAlert(activity, getString(R.string.please_press_on_send_again))
                }
            }
        }

    }

    fun sendOtp() {
        storeUserData.setBoolean(Constants.IS_OTP_SENT, true)
        progressDialog.show()
        val result = Utils.CallApi(activity).sendOtp(countryCode, phoneNumber)
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    Log.i("TAG", "onResponse: " + response.code())
                    if (response.code() != 200) {
                        serverAlert(activity)
                        if (progressDialog.isShowing) progressDialog.dismiss()
                        return
                    }
                    val res = response.body()!!.string()
                    Log.i("TAG", "onResponse: '$res")
                    if (progressDialog.isShowing) progressDialog.dismiss()
                    buttonResend.isEnabled = true
                    buttonVerifyPhone.isEnabled = true
                    try {
                        val `object` = JSONObject(res)
                        if (`object`.getBoolean("success")) {
                            fieldVerificationCode.requestFocus()
                            storeUserData.setString(Constants.SESSION_ID, `object`.getString("session_id"))
                            Utils.showToast(activity, R.string.four_digit_code_sent)
                        } else {
                            showAlert(activity, `object`.getString("message"))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                    serverAlert(activity)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }
        })
    }

    private fun verifyOTP() {
        progressDialog.show()
        val result = Utils.CallApi(activity).verifyOtp(
                fieldVerificationCode.text.toString().trim(),
                storeUserData.getString(Constants.SESSION_ID)
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                try {
                    Log.i("TAG", "onResponse: " + response.code())
                    if (response.code() != 200) {
                        serverAlert(activity)
                        return
                    }
                    val res = response.body()!!.string()
                    Log.i("TAG", "onResponse: '$res")
                    val `object` = JSONObject(res)
                    if (`object`.getBoolean("success")) {
                        storeUserData.setBoolean(Constants.IS_OTP_SENT, false)
                        storeUserData.setString(Constants.USER_COUNTRY_CODE, countryCode)
                        storeUserData.setString(Constants.USER_PHONE_NUMBER, phoneNumber)
                        startActivity(Intent(activity, RegisterActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                    } else {
                        showAlert(activity, getString(R.string.invaild_code))
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: JSONException) {
                    e.printStackTrace()
                    serverAlert(activity)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }
        })
    }

    private fun firebasePhoneAuth(phoneNumber: String) {
        storeUserData.setBoolean(Constants.IS_OTP_SENT, true)

        progressDialog.show()
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,  // Phone number to verify
                30,  // Timeout duration
                TimeUnit.SECONDS,  // Unit of timeout
                this,  // Activity (for callback binding)
                object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                        Log.d(TAG, "onVerificationCompleted:$phoneAuthCredential")
                        fieldVerificationCode.setText(phoneAuthCredential.smsCode)
                        if (progressDialog.isShowing) {
                            progressDialog.dismiss()
                        }

                        signInWithPhoneAuthCredential(phoneAuthCredential)

                    }

                    override fun onVerificationFailed(e: FirebaseException) {
                        Log.w(TAG, "onVerificationFailed", e)
                        if (progressDialog.isShowing) {
                            progressDialog.dismiss()
                        }
                        buttonResend.isEnabled = true
                        buttonVerifyPhone.isEnabled = true
                        if (e is FirebaseAuthInvalidCredentialsException) { // Invalid request
                            Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
                        } else if (e is FirebaseTooManyRequestsException) { // The SMS quota for the project has been exceeded // ...
                            //Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
                            sentTime = 0
                            buttonResend.performClick()
                        }
                    }

                    override fun onCodeSent(verificationId: String, forceResendingToken: PhoneAuthProvider.ForceResendingToken) {
                        super.onCodeSent(verificationId, forceResendingToken)
                        // The SMS verification code has been sent to the provided phone number, we
                        // now need to ask the user to enter the code and then construct a credential
                        // by combining the code with a verification ID.
                        Log.d(TAG, "onCodeSent:$verificationId")
                        mVerificationId = verificationId
                        if (progressDialog.isShowing) {
                            progressDialog.dismiss()
                        }
                        buttonResend.isEnabled = true
                        buttonVerifyPhone.isEnabled = true
                        // Save verification ID and resending token so we can use them later
                        //mVerificationId = verificationId;
                        //mResendToken = forceResendingToken;
                        fieldVerificationCode.requestFocus()
                        Utils.showToast(activity, R.string.four_digit_code_sent)
                    }
                }) // OnVerificationStateChangedCallbacks
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(
                        this
                ) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.i("TAG", "signInWithCredential:success")
                        val user = task.result!!.user

                        storeUserData.setBoolean(Constants.IS_OTP_SENT, false)
                        storeUserData.setString(Constants.USER_COUNTRY_CODE, countryCode)
                        storeUserData.setString(Constants.USER_PHONE_NUMBER, phoneNumber)
                        startActivity(Intent(activity, RegisterActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                        // ...
                    } else {
                        // Sign in failed, display a message and update the UI
                        Log.i("TAG", "signInWithCredential:failure " + task.exception)
                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                        }
                        showAlert(activity, getString(R.string.invaild_code))
                    }
                }
    }

}