package com.ta.news.activity.post

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.activity.MainActivity
import com.ta.news.pojo.NewsPojo
import com.ta.news.pojo.Spam
import com.ta.news.pojo.SpamPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_spam.*
import kotlinx.android.synthetic.main.fragment_spam_category.*
import kotlinx.android.synthetic.main.row_spam_category.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class SpamActivity : BaseActivity() {
    var selectedId = -1
    lateinit var pojo: NewsPojo
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_spam)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        pojo = intent.getSerializableExtra("pojo") as NewsPojo

        tvTitle.text = pojo.newsTitle
        tvDescription.text = pojo.newsDetails
        if (intent.getBooleanExtra("uploaded", false)) {
            tvAuthor.text = String.format("%s %s", storeUserData.getString(Constants.USER_FIRST_NAME), storeUserData.getString(Constants.USER_LAST_NAME))
        } else {
            tvAuthor.text = String.format("%s %s", pojo.firstName, pojo.lastName)
        }
        getSpamCategories()

        btnSubmit.setOnClickListener {
            if (selectedId == -1) {
                showAlert(activity, getString(R.string.pls_select_spam_cat))
            } else if (Utils.isEmpty(message)) {
                showAlert(activity, getString(R.string.write_spam_message))
            } else {
                postSpam()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            if (intent.getIntExtra("openCmd", -1) == 6) {
                startActivity(Intent(activity, MainActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
            } else {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getSpamCategories() {
        val result = Utils.CallApi(activity).getSpamCategory()
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() != 200) {
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val spam = Gson().fromJson(res, Spam::class.java)
                    selectReason.setOnClickListener {
                        SpamFragment(this@SpamActivity, spam).show(
                                supportFragmentManager,
                                SpamFragment::class.java.simpleName
                        )
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()

            }
        })

    }

    private fun postSpam() {
        showProgress()
        val result = Utils.CallApi(activity).postSpam(
                storeUserData.getString(Constants.USER_ID),
                pojo.postId,
                selectedId,
                message.text.toString()
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                dismissProgress()
                if (response.code() != 200) {
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val spam = Gson().fromJson(res, Spam::class.java)
                    showAlert(activity, spam.message, spam.success)
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()

            }
        })

    }
}

class SpamFragment(var activity: SpamActivity, var spam: Spam) : BottomSheetDialogFragment() {
    var bottomSheetDialog: BottomSheetDialog? = null
    lateinit var bottomSheet: FrameLayout

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        bottomSheetDialog!!.setOnShowListener { mDialog: DialogInterface ->
            val dialog = mDialog as BottomSheetDialog
            bottomSheet = dialog.findViewById(R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).state =
                    BottomSheetBehavior.STATE_EXPANDED
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).skipCollapsed = true
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).isHideable = true
            val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<FrameLayout>(bottomSheet)
        }
        bottomSheetDialog!!.setOnDismissListener { dialog: DialogInterface? -> }
        return bottomSheetDialog!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                DialogFragment.STYLE_NORMAL,
                R.style.CustomBottomSheetDialogTheme
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_spam_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvDistrict.adapter = SpamAdapter(activity, spam.data)
    }

    inner class SpamAdapter(var activity: Activity, var list: ArrayList<SpamPojo>) : RecyclerView.Adapter<SpamAdapter.ItemsViewHolder>() {

        override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
            val pojo = list[position]
            itemsViewHolder.itemView.tvTitle.text = pojo.name
            itemsViewHolder.itemView.mainLayout.setOnClickListener {
                (activity as SpamActivity).selectedId = pojo.id
                (activity as SpamActivity).selectReason.text = pojo.name
                bottomSheetDialog!!.dismiss()
            }
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
            val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_spam_category, viewGroup, false)
            return ItemsViewHolder(v)
        }

        override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
            super.onAttachedToRecyclerView(recyclerView)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        inner class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    }
}