package com.ta.news.activity

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.core.content.pm.PackageInfoCompat
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.facebook.ads.AdSettings
import com.facebook.ads.AudienceNetworkAds
import com.google.android.gms.tasks.Task
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.GsonBuilder
import com.ta.news.BuildConfig
import com.ta.news.R
import com.ta.news.activity.auth.PhoneAuthActivity
import com.ta.news.activity.auth.RegisterActivity
import com.ta.news.activity.post.NewsDetailActivity
import com.ta.news.controls.CustomDialog
import com.ta.news.pojo.SplashScreens
import com.ta.news.pojo.StateList
import com.ta.news.utils.Constants
import com.ta.news.utils.RetrofitHelper
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_splash.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.*
import java.lang.reflect.Modifier


class SplashActivity : BaseActivity() {

    var SPLASH_TIME = 6000
    var fromAds = false
    var pInfo: PackageInfo? = null
    var handler: Handler = Handler()
    var taskIsSuccessful = false
    var runnable: Runnable = Runnable {
        if (storeUserData.getString(Constants.STATE_LIST).isNotEmpty() && storeUserData.getString(Constants.USER_STATE).isNotEmpty()) {
            var reader = StringReader(storeUserData.getString(Constants.STATE_LIST))
            var gson = GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create()
            var states = gson.fromJson(reader, StateList::class.java)
            for (state in states.data) {
                if (storeUserData.getString(Constants.USER_STATE) == state.nm) {
                    if (!state.live) {
                        val dialog = CustomDialog(activity)
                        dialog.show()
                        dialog.setCancelable(false)
                        dialog.setMessage(state.down)
                        dialog.setPositiveButton(R.string.ok, View.OnClickListener { v: View? -> finish() })
                        return@Runnable
                    }
                }
            }
        }
        if (storeUserData.getBoolean(Constants.IS_OTP_SENT)) {
            startActivity(Intent(activity, PhoneAuthActivity::class.java))
        } else if (storeUserData.getString(Constants.USER_ID).isEmpty()) {
          /*  if (storeUserData.getString(Constants.USER_STATE).isEmpty()) {
                startActivity(Intent(activity, SelectStateActivity::class.java))
                finish()
            } else*/ if (storeUserData.getString(Constants.USER_PHONE_NUMBER).isNotEmpty()) {
                startActivity(Intent(activity, RegisterActivity::class.java))
                finish()
            } else {
                startActivity(Intent(activity, PhoneAuthActivity::class.java))
                finish()
            }
        } else {
            if (intent.data?.path != null) {
                if (intent.data?.toString()!!.startsWith("com.piplanapane://news-detail/")) {
                    startActivity(
                            Intent(activity, NewsDetailActivity::class.java)
                                    .putExtra("openCmd", 6)
                                    .putExtra("postId", intent.data?.path?.replace("/", "")?.toInt())
                    )
                } else {
                    startActivity(Intent(activity, MainActivity::class.java))
                }
            } else {
                startActivity(Intent(activity, MainActivity::class.java))
            }
            finish()
        }
    }

    private var mFirebaseRemoteConfig: FirebaseRemoteConfig? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_splash)
        AudienceNetworkAds.initialize(this)
        AdSettings.setVideoAutoplay(false)

        var string = "asdAasdAAAઆર્ટकला"
        Log.i("TAG", "onCreate: " + string.matches(Regex("[{A-Za-z\\p{InGUJARATI}\\p{InDEVANAGARI}}]+")))

        if (storeUserData.getString(Constants.USER_LANGUAGE).isEmpty()) {
            startActivity(Intent(activity, LanguageActivity::class.java))
            finish()
            return
        }

        if (storeUserData.getInt(Constants.SPLASH_IMAGE_TIME) > 0) {
            SPLASH_TIME = storeUserData.getInt(Constants.SPLASH_IMAGE_TIME)
        }

        try {
            pInfo = packageManager.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        checkRemoteConfig()
        if (!TextUtils.isEmpty(storeUserData.getString(Constants.SPLASH_DATA))) {
            val reader: Reader = StringReader(storeUserData.getString(Constants.SPLASH_DATA))
            val gson = GsonBuilder()
                    .create()
            val splashScreens = gson.fromJson(reader, SplashScreens::class.java)
            if (storeUserData.getInt(Constants.LAST_SPLASH) > splashScreens.splashArray.size - 1 || storeUserData.getInt(Constants.LAST_SPLASH) == -1) {
                storeUserData.setInt(Constants.LAST_SPLASH, 0)
            }
            if (splashScreens.splashArray.isEmpty()) {
                Glide.with(activity).load(R.drawable.splash).apply(RequestOptions())
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .transition(GenericTransitionOptions.with(R.anim.splash_anim))
                        .into(splash)
            } else {
                val advUrl = splashScreens.splashArray[storeUserData.getInt(Constants.LAST_SPLASH)].androidAdvUrl
                val url = splashScreens.splashArray[storeUserData.getInt(Constants.LAST_SPLASH)].image
                val fileName = url.substring(url.lastIndexOf('/') + 1)
                val splashImage = File(filesDir.absolutePath + "/" + fileName)
                if (splashImage.exists()) {
                    Glide.with(activity).load(splashImage)
                            .apply(RequestOptions())
                            .listener(object : RequestListener<Drawable?> {
                                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable?>, isFirstResource: Boolean): Boolean {
                                    Log.i("TAG", "onLoadFailed: 404")
                                    e!!.printStackTrace()
                                    splashImage.delete()
                                    return false
                                }

                                override fun onResourceReady(resource: Drawable?, model: Any, target: Target<Drawable?>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                                    Log.i("TAG", "onResourceReady: ")
                                    return false
                                }
                            })//.transition(DrawableTransitionOptions.withCrossFade())
                            .transition(GenericTransitionOptions.with(R.anim.splash_anim))
                            .into(splash)
                } else {
                    Glide.with(activity).load(url.replace(" ".toRegex(), "%20"))
                            .apply(RequestOptions())
                            .error(R.drawable.splash)
                            .listener(object : RequestListener<Drawable> {
                                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                                    Log.i("TAG", "onResourceReady: ")
                                    val splashImage = File(filesDir.absolutePath + "/" + fileName)
                                    var fOut: OutputStream? = null
                                    try {
                                        fOut = FileOutputStream(splashImage)
                                    } catch (e: FileNotFoundException) {
                                        e.printStackTrace()
                                    }
                                    (resource as BitmapDrawable).bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                                    try {
                                        fOut!!.flush() // Not really required
                                        fOut.close()
                                    } catch (e: IOException) {
                                        e.printStackTrace()
                                    }
                                    return false
                                }

                                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                                    //e!!.printStackTrace()
                                    //runOnUiThread { Glide.with(activity).load(R.drawable.splash).transition(GenericTransitionOptions.with(R.anim.splash_anim)).into(splash) }
                                    return false
                                }

                            })
                            //.transition(DrawableTransitionOptions.withCrossFade())
                            .transition(GenericTransitionOptions.with(R.anim.splash_anim))
                            .into(splash)
                }
                storeUserData.setInt(Constants.LAST_SPLASH, storeUserData.getInt(Constants.LAST_SPLASH) + 1)
                if (!TextUtils.isEmpty(advUrl)) {
                    splash.setOnClickListener {
                        handler.removeCallbacks(runnable)
                        fromAds = true
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(advUrl))
                        startActivity(browserIntent)
                    }
                }
            }
        } else {
            Glide.with(activity).load(R.drawable.splash).transition(DrawableTransitionOptions.withCrossFade()).transition(GenericTransitionOptions.with(R.anim.splash_anim)).into(splash)
        }
        /*if (storeUserData.getString(Constants.USER_STATE).isNotEmpty()) {
            handler.postDelayed(runnable, SPLASH_TIME.toLong())
        }*/
    }

    private fun checkRemoteConfig() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .build()
        mFirebaseRemoteConfig?.setConfigSettingsAsync(configSettings)
        var cacheExpiration: Long = 60 // 15 minutes in seconds.
        if (storeUserData.getString(Constants.STATE_LIST).isEmpty()) {
            cacheExpiration = 0
        }
        mFirebaseRemoteConfig!!.fetch(cacheExpiration)
                .addOnCompleteListener(this) { task: Task<Void?> ->
                    Log.d("RemoteConfig", "${task.isSuccessful}")
                    taskIsSuccessful = task.isSuccessful
                    if (taskIsSuccessful) {
                        mFirebaseRemoteConfig!!.activateFetched()
                        storeUserData.setInt(Constants.SPLASH_IMAGE_TIME, if (mFirebaseRemoteConfig!!.getString("splashTime").trim().isEmpty()) 6000 else mFirebaseRemoteConfig!!.getString("splashTime").toInt())
                        storeUserData.setInt(Constants.LIVE_VERSION, if (mFirebaseRemoteConfig!!.getString("androidLiveVersionCode").trim().isEmpty()) BuildConfig.VERSION_CODE else mFirebaseRemoteConfig!!.getString("androidLiveVersionCode").toInt())
                        storeUserData.setBoolean(Constants.FORCE_UPDATE, mFirebaseRemoteConfig!!.getBoolean("isAndroidForceUpdate"))
                        storeUserData.setString(Constants.URL, mFirebaseRemoteConfig!!.getString("baseUrl"))
                        storeUserData.setString(Constants.SHARE_TEXT, mFirebaseRemoteConfig!!.getString("shareText"))
                        storeUserData.setString(Constants.STATE_LIST, mFirebaseRemoteConfig!!.getString("stateList"))
                        storeUserData.setBoolean(Constants.IS_FACEBOOK, mFirebaseRemoteConfig!!.getBoolean("isFacebookAds"))
                        if (storeUserData.getString(Constants.USER_STATE).isNotEmpty()) {
                            var reader = StringReader(storeUserData.getString(Constants.STATE_LIST))
                            var gson = GsonBuilder()
                                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                    .serializeNulls()
                                    .create()
                            var states = gson.fromJson(reader, StateList::class.java)
                            for (state in states.data) {
                                if (storeUserData.getString(Constants.USER_STATE) == state.nm) {
                                    storeUserData.setString(Constants.URL, state.url);
                                }
                            }
                            //getImage()
                        }
                        /**
                         * STAGING
                         * */
                        //storeUserData.setString(Constants.URL,"http://5.9.62.174/staging/")
                        Log.i("TAG", "onComplete: ")
                    } else {
                        Log.i("TAG", "false")
                        //Toast.makeText(activity, "Retrying to fetch data!", Toast.LENGTH_SHORT).show();
                    }
                }
        handler.postDelayed(runnable, SPLASH_TIME.toLong())
        if (storeUserData.getString(Constants.USER_ID).isNotEmpty() && storeUserData.getString(Constants.STATE_LIST).isNotEmpty()) {
            getImage()
        }
    }

    override fun onResume() {
        super.onResume()
        if (fromAds) {
            handler.postDelayed(runnable, 400)
        }
    }

    private fun getImage() {
        val retrofitHelper = RetrofitHelper(activity)
        var call: Call<ResponseBody> =
                retrofitHelper.api().splashScreen(storeUserData.getString(Constants.USER_ID), if (Build.VERSION.SDK_INT > 27) PackageInfoCompat.getLongVersionCode(pInfo!!).toInt() else pInfo!!.versionCode)

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                val responseData = body.body()!!.string()
                Log.i("response", "onResponse: $responseData")
                try {
                    if (responseData.isNotEmpty()) {
                        val jsonObject = JSONObject(responseData)
                        if (jsonObject.getBoolean("success")) {
                            Constants.MAX_MEDIA_UPLOAD = jsonObject.getInt("maxMediaUpload")
                            storeUserData.setString(Constants.SPLASH_DATA, jsonObject.toString())
                            if (jsonObject.has("user") && jsonObject.get("user") is JSONObject) {
                                val user = jsonObject.getJSONObject("user")
                                storeUserData.setString(Constants.USER_FIRST_NAME, user.getString("firstName"))
                                storeUserData.setString(Constants.USER_LAST_NAME, user.getString("lastName"))
                                storeUserData.setString(Constants.USER_EMAIL, user.getString("email"))
                                storeUserData.setString(Constants.USER_COUNTRY_CODE, user.optString("countryCode"))
                                storeUserData.setString(Constants.USER_PHONE_NUMBER, user.getString("mobileNo"))
                                storeUserData.setString(Constants.USER_ADDRESS, user.getString("address"))
                                storeUserData.setString(Constants.USER_PINCODE, user.getString("pincode"))
                                storeUserData.setString(Constants.USER_IMAGE, user.getString("profileImage"))
                                storeUserData.setString(Constants.USER_TYPE_ID, user.getString("userTypeId"))
                                storeUserData.setString(Constants.REASON_MESSAGE, user.optString("reasonMessage"))
                                storeUserData.setInt(Constants.USER_STATUS, user.getInt("status"))
                                storeUserData.setBoolean(Constants.IS_PREMIUM, user.getInt("isPremium") == 1)

                                if (storeUserData.getInt(Constants.USER_STATUS) == 0) {
                                    storeUserData.clearData(activity)
                                    showAlert(activity, storeUserData.getString(Constants.REASON_MESSAGE))
                                }
                            }
                            Constants.androidShowAds = jsonObject.getBoolean("androidShowAds")
                            Constants.interstitialCount = jsonObject.getInt("interstitialCount")
                            Constants.supportContact = jsonObject.getString("supportContact")
                        } else {
                            handler.removeCallbacks(runnable)
                            if (!jsonObject.isNull("user") && jsonObject.getJSONObject("user").getBoolean("isBlocked")) {
                                storeUserData.clearData(activity)
                            }
                            showAlert(activity, jsonObject.getString("message"))
                        }
                    }
                } catch (e: Exception) {
                    justForError(e.message.toString() + "\n\n\n" + responseData)
                }
            }

            override fun onError(code: Int, error: String) {
                Log.i("TAG", "onError: $error")
                justForError(error)
            }
        })
    }


    private fun justForError(error: String) {
        val retrofitHelper = RetrofitHelper(activity)
        var call: Call<ResponseBody> =
                retrofitHelper.api().splashScreen(
                        storeUserData.getString(Constants.USER_ID),
                        if (Build.VERSION.SDK_INT > 27) PackageInfoCompat.getLongVersionCode(pInfo!!).toInt() else pInfo!!.versionCode, error
                )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                val responseData = body.body()!!.string()
                Log.i("response", "onResponse: $responseData")
                try {
                    if (responseData.isNotEmpty()) {
                        val jsonObject = JSONObject(responseData)
                        if (jsonObject.getBoolean("success")) {
                            Constants.MAX_MEDIA_UPLOAD = jsonObject.getInt("maxMediaUpload")
                            storeUserData.setString(Constants.SPLASH_DATA, jsonObject.toString())
                            if (jsonObject.has("user") && jsonObject.get("user") is JSONObject) {
                                val user = jsonObject.getJSONObject("user")
                                storeUserData.setString(Constants.USER_FIRST_NAME, user.getString("firstName"))
                                storeUserData.setString(Constants.USER_LAST_NAME, user.getString("lastName"))
                                storeUserData.setString(Constants.USER_EMAIL, user.getString("email"))
                                storeUserData.setString(Constants.USER_COUNTRY_CODE, user.optString("countryCode"))
                                storeUserData.setString(Constants.USER_PHONE_NUMBER, user.getString("mobileNo"))
                                storeUserData.setString(Constants.USER_ADDRESS, user.getString("address"))
                                storeUserData.setString(Constants.USER_PINCODE, user.getString("pincode"))
                                storeUserData.setString(Constants.USER_IMAGE, user.getString("profileImage"))
                                storeUserData.setString(Constants.USER_TYPE_ID, user.getString("userTypeId"))
                                storeUserData.setString(Constants.REASON_MESSAGE, user.optString("reasonMessage"))
                                storeUserData.setInt(Constants.USER_STATUS, user.getInt("status"))
                                storeUserData.setBoolean(Constants.IS_PREMIUM, user.getInt("isPremium") == 1)

                                if (storeUserData.getInt(Constants.USER_STATUS) == 0) {
                                    storeUserData.clearData(activity)
                                    showAlert(activity, storeUserData.getString(Constants.REASON_MESSAGE))
                                }
                            }
                            Constants.androidShowAds = jsonObject.getBoolean("androidShowAds")
                            Constants.interstitialCount = jsonObject.getInt("interstitialCount")
                            Constants.supportContact = jsonObject.getString("supportContact")
                        } else {
                            handler.removeCallbacks(runnable)
                            if (!jsonObject.isNull("user") && jsonObject.getJSONObject("user").getBoolean("isBlocked")) {
                                storeUserData.clearData(activity)
                            }
                            showAlert(activity, jsonObject.getString("message"))
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(code: Int, error: String) {
                Log.i("TAG", "onError: $error")
            }
        })
    }


}