package com.ta.news.fragment

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.GsonBuilder
import com.ta.news.R
import com.ta.news.pojo.District
import com.ta.news.pojo.DistrictPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.fragment_district_filter.*
import kotlinx.android.synthetic.main.row_district.view.*
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier


class DistrictFilterFragment(var itemClickListener: NewsFragment.ItemClickListener) : BottomSheetDialogFragment() {
    var bottomSheetDialog: BottomSheetDialog? = null
    lateinit var bottomSheet: FrameLayout
    lateinit var mActivity: FragmentActivity
    lateinit var storeUserData: StoreUserData
    lateinit var adapter: DistrictAdapter
    var list = ArrayList<DistrictPojo>()
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        bottomSheetDialog!!.setOnShowListener { mDialog: DialogInterface ->
            val dialog = mDialog as BottomSheetDialog
            bottomSheet = dialog.findViewById(R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).state =
                    BottomSheetBehavior.STATE_EXPANDED
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).skipCollapsed = true
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).isHideable = true
            val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<FrameLayout>(bottomSheet)
        }
        bottomSheetDialog!!.setOnDismissListener { dialog: DialogInterface? -> }
        return bottomSheetDialog!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                DialogFragment.STYLE_NORMAL,
                R.style.CustomBottomSheetDialogTheme
        )
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return inflater.inflate(R.layout.fragment_district_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val reader: Reader = StringReader(storeUserData.getString(Constants.DISTRICT_LIST))
        val gson = GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .create()
        var district = gson.fromJson(reader, District::class.java)
        var districtPojo = DistrictPojo()
        districtPojo.districtGu = getString(R.string.state)
        district.data.add(0, districtPojo)
        list.addAll(district.data)
        adapter = DistrictAdapter(mActivity, list)
        rvDistrict.adapter = adapter
    }


    inner class DistrictAdapter(var activity: Activity, var list: ArrayList<DistrictPojo>) : RecyclerView.Adapter<DistrictAdapter.ItemsViewHolder>() {

        override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
            val pojo = list[position]
            itemsViewHolder.itemView.tvTitle.text = pojo.districtGu
            itemsViewHolder.itemView.selected.isVisible = (storeUserData.getString(Constants.DISTRICT_ID).isEmpty() && position == 0) || (storeUserData.getString(Constants.DISTRICT_ID) == pojo.id.toString())
            itemsViewHolder.itemView.mainLayout.setOnClickListener {
                if (position == 0) {
                    storeUserData.setString(Constants.DISTRICT_ID, "")
                } else {
                    storeUserData.setString(Constants.DISTRICT_ID, pojo.id.toString())
                }
                itemClickListener.onDistrictSelect(pojo)
                bottomSheetDialog!!.dismiss()
            }
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
            val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_district, viewGroup, false)
            return ItemsViewHolder(v)
        }

        override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
            super.onAttachedToRecyclerView(recyclerView)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        inner class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    }
}