package com.ta.news.fragment

import android.app.Activity
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.facebook.ads.*
import com.google.android.gms.ads.AdRequest
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.controls.CustomProgressDialog
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import java.util.*

open class BaseFragment : Fragment() {

    lateinit var storeUserData: StoreUserData
    lateinit var mActivity: FragmentActivity
    lateinit var progressDialog: CustomProgressDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var language = StoreUserData(activity!!).getString(Constants.USER_LANGUAGE)
        if (language.isEmpty()) {
            language = "en"
        }
        val locale = Locale(language)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        activity!!.baseContext.resources.updateConfiguration(config,
                activity!!.baseContext.resources.displayMetrics)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    fun initProgress() {
        progressDialog = CustomProgressDialog(mActivity)
        progressDialog.setCancelable(false)
    }

    override fun onDestroyView() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
        super.onDestroyView()
    }

    fun showAlert(activity: Activity?, message: String?) {
        if (activity != null && !activity.isFinishing) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setCancelable(false)
            dialog.setMessage(message)
            dialog.setPositiveButton(R.string.ok)
        }
    }

    fun internetAlert(activity: Activity?) {
        if (activity != null && !activity.isFinishing) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setMessage(R.string.internet_error_message)
            dialog.setPositiveButton(R.string.ok)
        }
    }

    fun serverAlert(activity: Activity?) {
        if (activity != null && !activity.isFinishing) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setTitle(R.string.server_error_title)
            dialog.setMessage(R.string.server_error_message)
            dialog.setPositiveButton(R.string.ok)
        }
    }

    fun loadBannerAds(banner_container: LinearLayout, googleAdView: com.google.android.gms.ads.AdView, id: Int) {
        if (!storeUserData.getBoolean(Constants.IS_PREMIUM)) {
            var adView = AdView(mActivity, getString(id), AdSize.BANNER_HEIGHT_50)
            banner_container.addView(adView)
            var adListener = object : AdListener {
                override fun onAdClicked(p0: Ad?) {

                }

                override fun onError(p0: Ad?, p1: AdError?) {
                    googleAdView.visibility = View.VISIBLE
                    googleAdView.loadAd(AdRequest.Builder().build())
                }

                override fun onAdLoaded(p0: Ad?) {

                }

                override fun onLoggingImpression(p0: Ad?) {

                }
            }
            var buildConfig = adView.buildLoadAdConfig().withAdListener(adListener).build()
            adView.loadAd(buildConfig)
        } /*else {
            googleAdView.visibility = View.VISIBLE
            googleAdView.loadAd(AdRequest.Builder().build())
        }*/
    }
}