package com.ta.news.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.activeandroid.query.Select
import com.ta.news.R
import com.ta.news.adapter.SavedNewsAdapter
import com.ta.news.pojo.OfflineNews
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.fragment_saved.*

/**
 * Created by arthtilva on 06-Nov-16.
 */
class SavedNewsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_saved, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(mActivity)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    interface OnRefreshListener {
        fun onRefresh()
    }

    var refreshListener = object : OnRefreshListener {
        override fun onRefresh() {
            getData()
        }
    }

    fun getData() {
        val lst = Select().from(OfflineNews::class.java).orderBy("id desc").execute<OfflineNews>()
        val adapter = SavedNewsAdapter(mActivity, lst, refreshListener)
        if (lst.isEmpty()) {
            recyclerView.visibility = View.GONE
            ivNoInternet.visibility = View.VISIBLE
        }
        recyclerView.adapter = adapter
    }
}