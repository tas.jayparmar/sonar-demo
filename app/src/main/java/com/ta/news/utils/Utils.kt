package com.ta.news.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.ta.news.R
import com.ta.news.pojo.NewsPojo
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Utils {
    fun CallApi(activity: Activity): API {
        val okHttpClient = OkHttpClient()
        val TIMEOUT = 60 * 1000
        return Retrofit.Builder()
                .client(okHttpClient.newBuilder().connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).writeTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).build())
                .baseUrl(StoreUserData(activity).getString(Constants.URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(API::class.java)
    }

    fun CallApi(baseUrl: String?): API {
        val okHttpClient = OkHttpClient()
        val TIMEOUT = 60 * 1000
        return Retrofit.Builder()
                .client(okHttpClient.newBuilder().connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).writeTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).build())
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(API::class.java)
    }

    fun showToast(activity: Activity, message: Int) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    fun showToast(context: Context, message: Int) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun isOnline(context: Context): Boolean {
        val connectivity = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivity.allNetworkInfo
        for (i in info.indices) if (info[i].state == NetworkInfo.State.CONNECTED) {
            return true
        }
        return false
    }

    fun isValidEmail(email: EditText): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()
    }

    fun isEmpty(view: View?): Boolean {
        if (view is EditText) {
            if (view.text.toString().trim { it <= ' ' }.isEmpty()) {
                return true
            }
        } else if (view is Button) {
            if (view.text.toString().trim { it <= ' ' }.isEmpty()) {
                return true
            }
        }
        return false
    }

    fun hideKB(activity: Activity, view: View?) {
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun isAppInstalled(context: Context, uri: String): Boolean {
        val pm = context.packageManager
        var installed: Boolean
        installed = try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
        return installed
    }

    fun getUserDetails(context: Context): Bundle {
        var bundle = Bundle()
        var storeUserData = StoreUserData(context)
        bundle.putString("userId", storeUserData.getString(Constants.USER_ID))
        bundle.putString("mobileNumber", storeUserData.getString(Constants.USER_COUNTRY_CODE) + storeUserData.getString(Constants.USER_PHONE_NUMBER))
        bundle.putString("model", android.os.Build.MODEL)
        //bundle.putString("brand", android.os.Build.BRAND)
        //bundle.putString("hardware", android.os.Build.HARDWARE)
        //bundle.putString("manufacturer", android.os.Build.MANUFACTURER)
        //bundle.putString("product", android.os.Build.PRODUCT)
        //bundle.putString("device", android.os.Build.DEVICE)
        bundle.putInt("sdk", android.os.Build.VERSION.SDK_INT)
        return bundle
    }

    fun loadUserImage(context: Context, url: String, imageView: ImageView) {
        Glide.with(context).load(url)
                .apply(RequestOptions().placeholder(ContextCompat.getDrawable(context, R.drawable.user)).error(ContextCompat.getDrawable(context, R.drawable.user))
                        .centerCrop().transform(RoundedCorners(12))).into(imageView)
    }

    fun openShare(context: Context, pojo: NewsPojo) {
        var share: String
        share = pojo.newsTitle + "\n\n"
        var description = pojo.newsDetails
        if (description.length > 60) {
            description = description.substring(0, 55)
            description += "..."
        }
        share += description + "\n\n"
        share += "http://piplanapane.in/news/${pojo.postId}"
        share += "\n\n"
        share += ("વધુ માહિતી માટે અહીં થી એપ્લિકેશન ડાઉનલોડ કરો અને મફત સમાચાર મેળવો...\n"
                + "Android: http://bit.ly/2GC6LH1\niOS: https://apple.co/2Lv3hVm")
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_TEXT, share)
        context.startActivity(Intent.createChooser(sharingIntent, "Share using"))
    }
}