package com.ta.news.utils

import com.ta.news.pojo.BooksPojo
import com.ta.news.pojo.News.Ads
import com.ta.news.pojo.NewsPojo
import java.util.*

//contact details
//cron job mandi price
//about us save.
//add Support Contact Number
//instruction message to send news
//share text in admin panel
//topic and notification management in admin panel
//Title, Description, Image(Optional)
//Conditions - version,OS
//check premium feature

//addComment
//login register get and update profile
// sms apis

object Constants {
    const val TOPIC_NEWS = "TOPIC_NEWS"
    const val CMD_NEWS = 1
    const val CMD_BOOK = 2
    const val SINGLE_IMAGE = 3
    const val LOCAL_IMAGE = 4
    const val USER_FCM = "USER_FCM"
    const val NEWS_CATEGORY = "NEWS_CATEGORY" //YYYY-MM-dd
    const val CONTENT_CATEGORY = "CONTENT_CATEGORY" //YYYY-MM-dd
    const val DISTRICT_LIST = "DISTRICT_LIST"
    const val DISTRICT_ID = "DISTRICT_ID"
    const val NEWS_CAT_SYNC_DATE = "NEWS_CAT_SYNC_DATE" //YYYY-MM-dd
    const val CONTENT_CAT_SYNC_DATE = "CONTENT_CAT_SYNC_DATE" //YYYY-MM-dd
    const val RATE_V = "RATE_V"

    const val USER_ID = "USER_ID_V3"
    const val USER_COUNTRY_CODE = "USER_COUNTRY_CODE_V3"
    const val USER_PHONE_NUMBER = "USER_PHONE_NUMBER_V3"

    const val USER_FIRST_NAME = "USER_FIRST_NAME"
    const val USER_LAST_NAME = "USER_LAST_NAME"
    const val USER_EMAIL = "USER_EMAIL"
    const val USER_ADDRESS = "USER_ADDRESS"
    const val USER_CITY = "USER_CITY"
    const val USER_TALUKA = "USER_TALUKA"
    const val USER_PINCODE = "USER_PINCODE"
    const val USER_IMAGE = "USER_IMAGE"
    const val USER_TYPE_ID = "USER_TYPE_ID"
    const val USER_STATUS = "USER_STATUS"
    const val USER_LANGUAGE = "USER_LANGUAGE"
    const val USER_STATE = "USER_STATE"
    const val IS_PREMIUM = "IS_PREMIUM"
    const val IS_FACEBOOK = "IS_FACEBOOK"
    const val CONTACT_US_MESSAGE = "CONTACT_US_MESSAGE"
    const val SPLASH_DATA = "SPLASH_DATA"
    const val LAST_SPLASH = "SPLASH_LAST"
    const val SPLASH_IMAGE_TIME = "SPLASH_IMAGE_TIME"
    const val LIVE_VERSION = "LIVE_VERSION"
    const val FORCE_UPDATE = "FORCE_UPDATE"
    const val IS_SURVEY_FORM_AVAILABLE = "IS_SURVEY_FORM_AVAILABLE" //boolean
    const val IS_OTP_SENT = "IS_OTP_SENT" //boolean
    const val SESSION_ID = "SESSION_ID"
    const val REASON_MESSAGE = "REASON_MESSAGE"
    const val VERIFICATION_ID = "VERIFICATION_ID"
    var CATEGORY_BASE_URL = "CATEGORY_BASE_URL"
    const val URL = "URL"
    const val SHARE_TEXT = "SHARE_TEXT"
    const val STATE_LIST = "STATE_LIST"
    var lstNews = ArrayList<NewsPojo>()
    var lstBooks = ArrayList<BooksPojo>()
    var lstAdvs = ArrayList<Ads>()
    var MAX_MEDIA_UPLOAD = 3
    var IS_MULTIPLE_IMAGE_SNACK_SHOWN = "IS_MULTIPLE_IMAGE_SNACK_SHOWN"
    /*var BOOKS_BASE_URL = ""
    var COMMODITY_BASE_URL = ""
    var NEWS_BASE_URL = ""*/

    var androidShowAds = false
    var interstitialCount = 5
    var supportContact = ""
}