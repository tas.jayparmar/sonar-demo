package com.ta.news.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.pojo.CommodityPricePojo
import kotlinx.android.synthetic.main.row_commodity_price.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class CommodityPriceAdapter(var activity: Activity, var list: ArrayList<CommodityPricePojo>?) : RecyclerView.Adapter<CommodityPriceAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(userViewHolder: ItemsViewHolder, position: Int) {
        val pojo = list!![position]

        val fromDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val toDateFormat = SimpleDateFormat("dd-MM-yyyy")
        try {
            val date = fromDateFormat.parse(pojo.addDate)
            userViewHolder.itemView.tvDate.text = "${activity.getString(R.string.date)} : ${toDateFormat.format(date)}"
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        userViewHolder.itemView.tvName.text = "${pojo.districtGu} (${pojo.districtEn})"
        userViewHolder.itemView.tvVariety.text = "${pojo.marketGuName} (${pojo.marketEnName})"
        userViewHolder.itemView.tvMinPrice.text = "₹${pojo.minPrice} - ₹${pojo.maxPrice}"
        if (pojo.income.isEmpty()) {
            userViewHolder.itemView.tvIncome.text = ""
        } else {
            userViewHolder.itemView.tvIncome.text = "${activity.getString(R.string.income)} : ${pojo.income}"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        return ItemsViewHolder(LayoutInflater.from(activity).inflate(R.layout.row_commodity_price, parent, false))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}