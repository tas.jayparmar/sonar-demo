package com.ta.news.adapter

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.ImageActivity
import com.ta.news.activity.PdfActivity
import com.ta.news.pojo.MediaArrayBean
import com.ta.news.pojo.News
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_slider_image.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class ContentSubCategoryMediaImageAdapter(var activity: Activity, var list: ArrayList<MediaArrayBean>?) : RecyclerView.Adapter<ContentSubCategoryMediaImageAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val pojo = list!![position]
        when (pojo.type) {
            MediaArrayBean.IMAGE -> Glide.with(activity).load((StoreUserData(activity).getString(Constants.URL) + pojo.image).replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
            MediaArrayBean.VIDEO -> itemsViewHolder.itemView.image.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video))
            MediaArrayBean.PDF -> itemsViewHolder.itemView.image.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pdf))
            else -> {

            }
        }

        itemsViewHolder.itemView.image.setOnClickListener {
            if (pojo.type == MediaArrayBean.IMAGE) {
                activity.startActivity(Intent(activity, ImageActivity::class.java)
                        .putExtra("url", (StoreUserData(activity).getString(Constants.URL) + pojo.image).replace(" ".toRegex(), "%20"))
                        .putExtra("position", position)
                        .putExtra("cmd", Constants.SINGLE_IMAGE)
                )
            } else if (pojo.type == MediaArrayBean.PDF) {
                activity.startActivity(Intent(activity, PdfActivity::class.java)
                        .putExtra("url", (StoreUserData(activity).getString(Constants.URL) + pojo.image).replace(" ".toRegex(), "%20"))
                )
            } else {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                when (pojo.type) {
                    News.VIDEO -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + pojo.image), "video/*")
                    else -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + pojo.image), "*/*")
                }
                try {
                    activity.startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_slider_image, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}