package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.fragment.UploadedFragment
import com.ta.news.activity.post.CommentActivity
import com.ta.news.activity.MainActivity
import com.ta.news.activity.post.NewsDetailActivity
import com.ta.news.activity.auth.ProfileActivity
import com.ta.news.pojo.NewsPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.row_uploaded.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class UploadedAdapter(var activity: Activity, mRecyclerView: RecyclerView, var list: ArrayList<NewsPojo>, var onDeleteListener: UploadedFragment.OnDeleteListener) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1
    private var mOnLoadMoreListener: OnLoadMoreListener? = null
    private var isLoading = false
    private val visibleThreshold = 5
    private var lastVisibleItem = 0
    private var totalItemCount = 0


    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener?) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(activity).inflate(R.layout.row_uploaded, parent, false)
            return ItemsViewHolder(view)
        } else {
            val view = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false)
            return LoadingViewHolder(view)
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size
    }

    fun setLoaded() {
        isLoading = false
    }

    override fun getItemViewType(position: Int): Int {
        return if (list!![position] == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var progressBar: ProgressBar

        init {
            progressBar = itemView.findViewById<View>(R.id.progressBar) as ProgressBar
        }
    }

    init {
        val linearLayoutManager = mRecyclerView.layoutManager as LinearLayoutManager?
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager!!.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }

    override fun onBindViewHolder(itemsViewHolder: RecyclerView.ViewHolder, position: Int) {
        if (itemsViewHolder is ItemsViewHolder) {
            val userViewHolder = itemsViewHolder
            val `object` = list[position]


            if (`object`.firstName != null && `object`.firstName.isNotEmpty()) {
                itemsViewHolder.itemView.tvName.text = `object`.firstName + " " + `object`.lastName
            } else {
                itemsViewHolder.itemView.tvName.text = StoreUserData(activity).getString(Constants.USER_FIRST_NAME) + " " + StoreUserData(activity).getString(Constants.USER_LAST_NAME)
            }
            itemsViewHolder.itemView.tvName.setOnClickListener { v: View? -> activity.startActivity(Intent(activity, ProfileActivity::class.java).putExtra("userId", `object`.userId)) }

            Utils.loadUserImage(activity, StoreUserData(activity).getString(Constants.USER_IMAGE), itemsViewHolder.itemView.profileImage)
            itemsViewHolder.itemView.tvDate.text = `object`.addDate

            userViewHolder.itemView.tvTitle.text = `object`.newsTitle
            userViewHolder.itemView.tvDescription.text = `object`.newsDetails
            userViewHolder.itemView.tvStatus.text = `object`.status
            if (`object`.reason != null && `object`.reason.isNotEmpty()) {
                userViewHolder.itemView.tvReason.text = `object`.reason
                userViewHolder.itemView.tvReason.visibility = View.VISIBLE
            } else {
                userViewHolder.itemView.tvReason.visibility = View.GONE
            }
            when (`object`.statusCode) {
                0 -> userViewHolder.itemView.tvStatus.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.deleted))
                1 -> userViewHolder.itemView.tvStatus.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.accept))
                2 -> userViewHolder.itemView.tvStatus.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.decline))
                3 -> userViewHolder.itemView.tvStatus.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.pending))
                else -> {
                }
            }

            itemsViewHolder.itemView.viewPager.adapter = NewsMediaImageAdapter(activity, `object`.mediaArray)
            itemsViewHolder.itemView.viewPager.setHasFixedSize(true)
            itemsViewHolder.itemView.viewPager.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            if (`object`.mediaArray != null && `object`.mediaArray.size > 0) {
                itemsViewHolder.itemView.viewPager.visibility = View.VISIBLE
            } else {
                itemsViewHolder.itemView.viewPager.visibility = View.GONE
            }
            if (`object`.isVerified) {
                itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.verified), null)
            } else {
                itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            }
            itemsViewHolder.itemView.ivComment.isVisible = `object`.statusCode == 1
            itemsViewHolder.itemView.btnShare.isVisible = `object`.statusCode == 1
            //onClick
            itemsViewHolder.itemView.btnShare.setOnClickListener {
                Utils.openShare(activity, `object`)
            }
            itemsViewHolder.itemView.ivComment.setOnClickListener {

                activity.startActivity(Intent(activity, CommentActivity::class.java)
                        .putExtra("position", position)
                        .putExtra("pojo", `object`))


            }
            itemsViewHolder.itemView.mainLayout.setOnClickListener {
                when (`object`.statusCode) {
                    0 -> (activity as MainActivity).showAlert(activity, activity.getString(R.string.you_cant_see_removed_ads))//deleted
                    1 -> {
                        activity.startActivity(Intent(activity, NewsDetailActivity::class.java)
                                .putExtra("uploaded", true)
                                .putExtra("pojo", `object`)
                                .putExtra("position", position)
                        )
                    }
                    2 -> (activity as MainActivity).showAlert(activity,  activity.getString(R.string.you_cant_see_removed_ads))//declined
                    3 -> (activity as MainActivity).showAlert(activity, activity.getString(R.string.post_is_under_review))
                    else -> {
                    }
                }
            }
            itemsViewHolder.itemView.btnDeleteFromServer.setOnClickListener {
                if (`object`.statusCode == 1 || `object`.statusCode == 3) {
                    val dialog = CustomDialog(activity)
                    dialog.show()
                    dialog.setMessage(R.string.delete_news_confirmation)
                    dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                        dialog.dismiss()
                        onDeleteListener.onDelete(`object`.postId)
                    })
                    dialog.setNegativeButton(R.string.no, View.OnClickListener { dialog.dismiss() })
                } else if (`object`.statusCode == 2) {
                    (activity as MainActivity).showAlert(activity, activity.getString(R.string.post_already_declined))
                }
            }
        } else if (itemsViewHolder is LoadingViewHolder) {
            itemsViewHolder.progressBar.isIndeterminate = true
        }
    }
}