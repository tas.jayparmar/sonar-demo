package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.activity.auth.ProfileActivity
import com.ta.news.controls.CustomDialog
import com.ta.news.pojo.CommentPojo
import com.ta.news.pojo.NewsPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.layout_loading_item.view.*
import kotlinx.android.synthetic.main.row_comment.view.*
import kotlinx.android.synthetic.main.row_comment_current.view.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Arth on 29-Oct-17.
 */
class CommentAdapter(var activity: Activity, mRecyclerView: RecyclerView, var list: ArrayList<CommentPojo>, var newsPojo: NewsPojo) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_CURRENT = 1
    private val VIEW_TYPE_LOADING = 2
    var storeUserData: StoreUserData = StoreUserData(activity)
    lateinit var mOnLoadMoreListener: OnLoadMoreListener
    private var isLoading = false
    private val visibleThreshold = 5
    private var lastVisibleItem = 0
    private var totalItemCount = 0


    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    fun addItem(pojo: CommentPojo) {
        list.add(0, pojo)
        notifyItemInserted(0)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(activity).inflate(R.layout.row_comment, parent, false)
            return ItemsViewHolder(view)
        } else if (viewType == VIEW_TYPE_CURRENT) {
            val view = LayoutInflater.from(activity).inflate(R.layout.row_comment_current, parent, false)
            return ItemsViewHolderCurrent(view)
        } else {
            val view = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false)
            return LoadingViewHolder(view)
        }

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list.size
    }

    fun setLoaded() {
        isLoading = false
    }

    override fun getItemViewType(position: Int): Int {
        return if (storeUserData.getString(Constants.USER_ID).isEmpty()) {
            VIEW_TYPE_ITEM
        } else {
            if (list[position].isLoader) VIEW_TYPE_LOADING else if (storeUserData.getString(Constants.USER_ID).toInt() == list[position].userId) VIEW_TYPE_CURRENT else VIEW_TYPE_ITEM
        }
    }

    fun deleteComment(commentId: Int, position: Int) {
        val result = Utils.CallApi(activity).commentDelete(commentId, newsPojo.postId)
        result?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("TAG", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        list.removeAt(position)
                        notifyItemRemoved(position)
                    } else {
                        showAlert(activity, jsonObject.getString("message"))
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    fun showAlert(activity: Activity, message: String?) {
        val dialog = CustomDialog(activity)
        dialog.show()
        dialog.setCancelable(false)
        dialog.setMessage(message)
        dialog.setPositiveButton(R.string.ok)
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class ItemsViewHolderCurrent(itemView: View) : RecyclerView.ViewHolder(itemView)

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        fun serverAlert(activity: Activity) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setTitle(R.string.server_error_title)
            dialog.setMessage(R.string.server_error_message)
            dialog.setPositiveButton(R.string.ok)
        }
    }

    init {
        val linearLayoutManager = mRecyclerView.layoutManager as LinearLayoutManager?
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager!!.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    mOnLoadMoreListener.onLoadMore()
                    isLoading = true
                }
            }
        })
    }

    override fun onBindViewHolder(itemsViewHolder: RecyclerView.ViewHolder, position: Int) {
        if (itemsViewHolder is ItemsViewHolder) {
            val `object` = list[position]
            itemsViewHolder.itemView.tvName.text = `object`.firstName + " " + `object`.lastName + " - " + (if (`object`.countryCode == null) "" else `object`.countryCode) + `object`.mobileNo
            itemsViewHolder.itemView.delete.isVisible = newsPojo.userId.toString() == storeUserData.getString(Constants.USER_ID)
            itemsViewHolder.itemView.tvDescription.text = `object`.comment
            itemsViewHolder.itemView.tvDate.text = SimpleDateFormat("MMM dd''yy hh:mm a", Locale.ENGLISH).format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(`object`.addDate))
            itemsViewHolder.itemView.whatsApp.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://api.whatsapp.com/send?phone=" + (if (`object`.countryCode.isNullOrEmpty()) "" else `object`.countryCode).replace("+", "") + `object`.mobileNo))
                activity.startActivity(browserIntent)
            }
            itemsViewHolder.itemView.corner.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.chat_corner))
            itemsViewHolder.itemView.tvName.setOnClickListener { activity.startActivity(Intent(activity, ProfileActivity::class.java).putExtra("userId", `object`.userId)) }
            if (`object`.isVerified == 1) {
                itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.verified), null)
            } else {
                itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            }
            itemsViewHolder.itemView.delete.setOnClickListener {
                val dialog = CustomDialog(activity)
                dialog.show()
                dialog.setCancelable(false)
                dialog.setMessage(activity.getString(R.string.confirm_delete_comment))
                dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                    dialog.dismiss()
                    deleteComment(`object`.id, position)
                })
            }
        } else if (itemsViewHolder is ItemsViewHolderCurrent) {
            val `object` = list[position]
            itemsViewHolder.itemView.tvNameCu.text = `object`.firstName + " " + `object`.lastName
            itemsViewHolder.itemView.tvDescriptionCu.text = `object`.comment
            itemsViewHolder.itemView.tvDateCu.text = `object`.addDate
            itemsViewHolder.itemView.cornerCu.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.chat_corner_current))
            itemsViewHolder.itemView.deleteCu.setOnClickListener { v: View? ->
                val dialog = CustomDialog(activity)
                dialog.show()
                dialog.setCancelable(false)
                dialog.setMessage(activity.getString(R.string.confirm_delete_comment))
                dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                    dialog.dismiss()
                    deleteComment(`object`.id, position)
                })

                dialog.setNegativeButton(R.string.no, View.OnClickListener { dialog.dismiss() })
            }
            itemsViewHolder.itemView.tvNameCu.setOnClickListener { v: View ->
                activity.startActivity(Intent(activity, ProfileActivity::class.java).putExtra("userId", `object`.userId))
            }
        } else if (itemsViewHolder is LoadingViewHolder) {
            itemsViewHolder.itemView.progressBar.isIndeterminate = true
        }
    }
}