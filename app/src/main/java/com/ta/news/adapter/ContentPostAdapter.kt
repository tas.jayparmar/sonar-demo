package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.content.ContentDetailActivity
import com.ta.news.pojo.ContentPost
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_content_post.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class ContentPostAdapter(var activity: Activity, var list: ArrayList<ContentPost>) : RecyclerView.Adapter<ContentPostAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        itemsViewHolder.itemView.tvName.text = `object`.title
        itemsViewHolder.itemView.tvDate.text = SimpleDateFormat("MMM dd''yy", Locale.ENGLISH).format(SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(`object`.addDate))
        itemsViewHolder.itemView.tvDescription.text = `object`.details
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL) + "" + `object`.thumbImage).into(itemsViewHolder.itemView.profileImage)
        if (`object`.mediaArray.isNotEmpty()) {
            itemsViewHolder.itemView.viewPager.visibility = View.VISIBLE
            itemsViewHolder.itemView.viewPager.adapter = ContentSubCategoryMediaImageAdapter(activity, `object`.mediaArray)
        } else {
            itemsViewHolder.itemView.viewPager.visibility = View.GONE
        }
        itemsViewHolder.itemView.setOnClickListener {
            activity.startActivity(Intent(activity, ContentDetailActivity::class.java).putExtra("content", `object`))
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_content_post, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}