package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.content.ContentCategoryActivity
import com.ta.news.pojo.ContentCategory
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_category.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class ContentCategoryAdapter(var activity: Activity, var list: ArrayList<ContentCategory>) : RecyclerView.Adapter<ContentCategoryAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        itemsViewHolder.itemView.tvTitle.text = `object`.name
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL) + `object`.image.replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)

        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            activity.startActivity(Intent(activity, ContentCategoryActivity::class.java)
                    .putExtra("categoryId", `object`.id)
                    .putExtra("name", `object`.name))

            //(activity as MainActivity).changeFragment(ContentSubCategoryFragment(), R.string.content, bundle, true)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_category, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}