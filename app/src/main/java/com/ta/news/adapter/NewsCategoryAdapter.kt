package com.ta.news.adapter

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.fragment.NewsFragment
import com.ta.news.activity.MainActivity
import com.ta.news.pojo.CategoryPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_category.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class NewsCategoryAdapter(var activity: Activity, var list: ArrayList<CategoryPojo>) : RecyclerView.Adapter<NewsCategoryAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        itemsViewHolder.itemView.tvTitle.text = `object`.categoryName
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.CATEGORY_BASE_URL) + `object`.image.replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)

        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("categoryId", `object`.id)
            (activity as MainActivity).changeFragment(NewsFragment(), R.string.news, bundle, true)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_category, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}