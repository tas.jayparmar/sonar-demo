package com.ta.news.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.pojo.CategoryPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_featured_category.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class SelectFeaturedCategoryAdapter(var activity: Activity, var list: ArrayList<CategoryPojo>) : RecyclerView.Adapter<SelectFeaturedCategoryAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        itemsViewHolder.itemView.tvTitle.text = `object`.categoryName
        itemsViewHolder.itemView.select.isVisible = list[position].isSelected
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.CATEGORY_BASE_URL) + `object`.image.replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
        itemsViewHolder.itemView.mainLayout.setOnClickListener { v ->
            list[position].isSelected = !list[position].isSelected
            notifyDataSetChanged()
        }
    }

    fun getSelectedCategories(): String {
        var sel = ""
        for (pojo in list) {
            if (pojo.isSelected) {
                sel += "${pojo.id},"
            }
        }
        if (sel.isNotEmpty()) {
            sel = sel.substring(0, sel.length - 1)
        }
        return sel
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_featured_category, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}