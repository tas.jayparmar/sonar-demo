package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.CommodityPriceActivity
import com.ta.news.pojo.CommodityPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_commodity.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class CommodityAdapter(var activity: Activity, var list: ArrayList<CommodityPojo>?) : RecyclerView.Adapter<CommodityAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(userViewHolder: ItemsViewHolder, position: Int) {
        val pojo = list!![position]
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL)  + pojo.commodityImage).into(userViewHolder.itemView.image)
        userViewHolder.itemView.tvVariety.text = "${pojo.commodity}(${pojo.variety})"
        userViewHolder.itemView.tvName.text = "${pojo.nameGu}"
        userViewHolder.itemView.setOnClickListener {
            //onClick
            activity.startActivity(Intent(activity, CommodityPriceActivity::class.java)
                    .putExtra("commodityId", pojo.id)
                    .putExtra("commodityName", pojo.nameGu)
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        val view = LayoutInflater.from(activity).inflate(R.layout.row_commodity, parent, false)
        return ItemsViewHolder(view)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}