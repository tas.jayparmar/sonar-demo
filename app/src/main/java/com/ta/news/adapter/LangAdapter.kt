package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.activity.SplashActivity
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_state.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class LangAdapter(var activity: Activity, var list: Array<String>, var codeList: Array<String>) : RecyclerView.Adapter<LangAdapter.ItemsViewHolder>() {
    var storeUserData = StoreUserData(activity)

    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        itemsViewHolder.itemView.tvTitle.text = list[position]
        itemsViewHolder.itemView.setOnClickListener {
            storeUserData.setString(Constants.USER_LANGUAGE, codeList[position])
            Log.i("TAG", "onBindViewHolder: "+codeList[position])
            activity.startActivity(Intent(activity, SplashActivity::class.java))
            activity.finish()
        }


    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        return ItemsViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.row_state, viewGroup, false))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}